<#
 # Description : Secure Script to Run Cloud Image
 # Created : 06/22/2024 by Danial
 #>

 param(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [string]$Key
)

# Convert the string key to a byte array
$KeyBytes = [System.Text.Encoding]::UTF8.GetBytes($Key)
# Ensure the key is exactly 32 bytes (256 bits)
if ($KeyBytes.Length -ne 32) {
    throw "The provided key must be exactly 32 bytes long when converted to UTF-8."
}

# Function to securely decrypt strings
function Decrypt-SecureString {
    param ([string]$EncryptedString)
    $secureString = ConvertTo-SecureString $EncryptedString -Key $KeyBytes
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureString)
    [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
}

# Function to display banner
function Show-Banner {
    Clear-Host
    Write-Host ("=" * 73) -ForegroundColor Cyan
    Write-Host "===================== Cloud Image Deployment Script =====================" -ForegroundColor Cyan
    Write-Host ("=" * 73) -ForegroundColor Cyan
    Write-Host "========================== Starting Imaging ZTI =========================" -ForegroundColor Cyan
    Write-Host "================= Edition - 24H2 == Build - 26100.3476 ==================" -ForegroundColor Cyan
    Write-Host ("=" * 73) -ForegroundColor Cyan
    Start-Sleep -Seconds 5
}

# Function to update OSD module
function Update-OSDModule {
    $installedVersion = (Get-Module -ListAvailable OSD | Sort-Object Version -Descending | Select-Object -First 1).Version
    $onlineVersion = (Find-Module -Name OSD).Version

    if ($installedVersion -lt $onlineVersion) {
        Install-Module -Name OSD -Force
        Import-Module OSD
    }

    $updatedVersion = (Get-Module -ListAvailable OSD | Sort-Object Version -Descending | Select-Object -First 1).Version
    if ($updatedVersion -lt [version]'24.3.10.1') {
        Write-Host "The required module version is not loaded. Please check the network connection and try again!" -ForegroundColor Cyan
        exit
    }
}

# Function to set up deployment
function Set-DeploymentConfig {
    param (
        [string]$DeploymentType,
        [string]$EncryptedJsonUrl
    )

    $lineLength = 76
    $title = "{0} Deployment" -f $DeploymentType.PadLeft(7 + ($DeploymentType.Length - $DeploymentType.Length) / 2).PadRight(7)
    $padding = "=" * (($lineLength - $title.Length - 2) / 2)

    Write-Host ("=" * $lineLength) -ForegroundColor Cyan
    Write-Host "$padding $title $padding" -ForegroundColor Cyan
    Write-Host ("=" * $lineLength) -ForegroundColor Cyan

    if ($EncryptedJsonUrl) {
        $JsonUrl = Decrypt-SecureString $EncryptedJsonUrl
        Invoke-WebRequest -Uri $JsonUrl -OutFile X:\OSDCloud\Config\AutopilotJSON\AutopilotProfile.json
    }
}

# Main script
Update-OSDModule
Show-Banner

# Select Deployment Method
$actionChoice = @(
    [System.Management.Automation.Host.ChoiceDescription]::new("&Hybrid", "Hybrid Joined Machine")
    [System.Management.Automation.Host.ChoiceDescription]::new("&AAD", "AzureAD Joined Machine")
    [System.Management.Automation.Host.ChoiceDescription]::new("&Travel", "AzureAD Joined Machine for Travel")
    [System.Management.Automation.Host.ChoiceDescription]::new("&Donation PC", "Regular Image for Donation PC")
)

$action = $Host.UI.PromptForChoice("Deployment Method", "Select a Deployment method to perform imaging", $actionChoice, 0)

$Global:StartOSDCloudGUI = @{
    ZTI                  = $true
    HPIAALL              = $true
    HPIADrivers          = $false
    HPIAFirmware         = $false
    HPTPMUpdate          = $false
    HPBIOSUpdate         = $false
    OSName               = 'Windows 11 24H2 x64'
    OSEdition            = 'Pro'
    OSActivation         = 'Retail'
    OSLanguage           = 'en-us'
    updateDiskDrivers    = $true
    updateFirmware       = $true
    updateNetworkDrivers = $true
    updateSCSIDrivers    = $true
    SyncMSUpCatDriverUSB = $true
    WindowsUpdate        = $true
    WindowsUpdateDrivers = $false
}

# Encrypted URLs
$encryptedHybridUrl = "76492d1116743f0423413b16050a5345MgB8ADUAawBZAEYASwB6AHAAMAA3AEEAdgBEADIAQgB3AFoARAA2AFcAZgBBAH
cAPQA9AHwAOQBmAGMAOQA3AGUANgA5AGUAYQAyAGUANQA5AGQAMgAwAGEANQAxADcAZgBkADIAMQA3ADcAZQAyADQAZABhAGEAYgAyAGYAYwA4AGEAMQA
1AGQAMABhADQAMQAzAGUAOQA1AGMAMABmADgAZgA2ADcAMgA0ADAAZQA5ADIANwA1ADgAMgAyAGIAZAA1AGUAMgBlADYAMwBmADUANAA3ADQAMwAzAGUA
NAA3AGEAMQAyADYAMAA3AGMANQAxADgAMwA0ADEAMwBlADkAMQAyADMAOABmADYAYwA0ADEANABkADYAYQA2ADEAOABmADcANABlADcAZQA0ADYAMgBiA
DAAYwBmADYAYwAyAGIANABhAGEANwBiADMANQAwAGUAMQAxADEAZABiADIANwBhAGUAMQAzADcANQA5ADgAMAA4ADkANAA2ADAAOABkADIAMgBlADIAZQ
A0ADYAYwA4ADMAMQAyADAAYQA4AGEANQBjAGUAMQA2ADMAMgA3AGMANgA4AGIAOQA0ADEAMQA2ADQANABkADcAZAAzADIAYgBkADQAOQA5AGQANgAxAGQ
AOQA5AGQAOAA2ADgAOQA4ADAAOAAzADgAYQBjAGYAOAAyADgANwBiAGIAYgAyAGUAZgBiAGMAMwA5AGMAZQA0AGMAMgBlAGIAOAA1ADgAZQBlADUANgAz
AGUAOAAxADYAOAAwADIANgBhAGUAOAA5ADMAYgA3AGQAZABhADAAMwBkADYAMwA3AGUAZQA2AGYAMQA4ADEAZQBjADIANgBmADcAZABjADUAYgBiAGUAZ
QA5AGUANgBjAGQANwAwADUAYgBjAGMAOQA2AGIAYQBlADUAOQBiADIAYQBlAGMAYQBjAGEANwBjAGEAYwA1AGEANwAwADMAMAAwADAAOQBjADEANQA5AG
MAMQA1ADcAYQA="
$encryptedAADUrl = "76492d1116743f0423413b16050a5345MgB8AG4ARgBiAEUAbgA1AHMAbwBmAEgATgBEAEkARABqADcAVgBsADEAZwBGAGcAP
QA9AHwAMAA4ADYAZQA1ADYAOAA0ADMAYwAzAGUAZAA2ADMAMABiADAAMQAzADkAYQBhADAAYQA1AGIANwAyADkAOQA3ADYAZAAzADUAOQA2ADUAMAA3AD
kAMQA2ADYAZgBlAGYANwA2ADMANQBjADAANgA3AGYAMQA0ADAANgA4ADkAOABiADkAZgAwADIAZgAwAGUAOQA5ADIAOAAxADgANwBjAGEAZQBmAGQAMwA
wADUAZQBmAGYAOAA4ADEAMwA5AGEAOAAwADQAYgA3ADQAOABkADcANgA3AGQAMAAyAGUAZgAyADcAMgAzADYAYwA2ADAAOAAwAGUANQA5ADMAMwBiAGEA
ZAAyADEAMwBlADAANAA1ADUAYQA1ADcANAAyADUAZQA3ADcAMgAwADYAYQA0ADEAYQA3ADQAMQAwADcANQBiADQAZABmAGYAZQA3ADMAYQBlADUANQA3A
GQAMgA3ADgAZABmAGEAYQAzADAAYQAyADQAZABiADQAZgBhADkANQAxAGYAOQBhADAAOQA4ADkAYwBhAGEAYgBhAGIAOABkADYANgBiAGMANgA3ADUAYw
BkADgANAAwAGQAMABjAGMAMwBjAGEAOAAyAGIANgAwADYAZABjAGEAMwA2AGUAMQBhAGQAMwA1AGEAZABjAGYAMgA4AGMAMgBkAGQANwBiADMAYwA1AGQ
AMwA2ADAAMQA0ADIAMQA4ADUAYgBkADgAYgAyADkAOQA0ADYAYwBkAGQAMQBhADAAZAAzADYAYgBhADQANwAzADIAZAA3AGMAZQBkADUAMgAwADEAZQAy
ADEAMwBlADkANQAxAGUAMAA4AGEAYgBlAGYAYwAzAGYANwBiAGEAYwBiAGIANQA0AGYAMgA2AGMAMgBjADcAYgAyAGUANQAzADAAZgBlADIAMABlAGQAY
QA3ADMAZAA="
$encryptedTravelUrl = "76492d1116743f0423413b16050a5345MgB8ADAAOQBnAFgATgAwAG4AWABSAHUAbABpADAAcQBRAHcARwBnAGMAUQBnAE
EAPQA9AHwAZgAxADkAMQA0ADQAMAA1ADYAMAAwADkAOAA0AGYAOABkADYANAA1AGIAYgBkADUAYQA2ADgANQAyADMAYgBlAGYAYgA3ADEAYwA4ADEAMgB
jAGIAMwA0AGYANQA3ADIAZQA1AGYAYQAyADcAYwBhAGUAMgAxAGUAOQBiADQAMgA0AGIANQA3ADcAOQAzADEAZABmADkANQBjADYAMAA0AGYAMgBkADQA
YQBjADAAYgA2ADAAOAA5ADQAOQBiADAAMwAzAGEAZABjAGYAYQBjADcANQBkADMAMgAyAGQAYgAzADMANQAwADEANABmAGYANwA4AGEAMQA1AGEANAA0A
GEAMwBjADIAYgA5ADQANgA5AGQANgA0ADQAYQAxAGYAZgA1ADAAZQA2ADgAOABjADIAZQBlADAANgBjAGIAMwA1ADcAZgBlADcAYwA5ADIAOQAyAGIAMw
AwADEAYwA2ADEAYwA4ADMAZQAzADEAMgBkADgAOABkAGMANgAxADUAMQBhAGUANAAwADcAZABmAGYAZQBkAGEANgA1ADIAYgAwADcAOAA5ADMANgA2ADc
AZABmADQANAA5ADEAZAAxAGMAOQAyAGUANAA2AGEAYQBhADAAYgAxAGQAYwA5AGEAZgAyADUAYgA4ADcAYgA5ADEAZQA1ADYAOAA0AGUAMQBhADEAYwBk
AGUAYQAxAGIAMgA0AGEAYQA4AGMAYwBmADEAMQA4ADAANABkAGYAMAA5AGEAMwAyAGMAMgA3ADkANABiADUAYgBkADAANgBlAGIANwA2ADMAYwBlADEAO
QAyAGMAMAA4ADMANwBiAGEAYgAwAGYANQA4AGIAMgAyADQANwBkAGEAZgA0ADkANQBlADIAYwAwADUAZAA0ADYAYQA0ADAAMABhAGUAZABiADYANwA2AG
QAMgA5AGIANAA="
$encryptedImageFileUrl = "76492d1116743f0423413b16050a5345MgB8AFQATQBFAEIAZQBMAHYANQBMAFUAbgBvAGcAcQB3AEMAWABNAHoAeAB
hAEEAPQA9AHwAMQAwAGIAMQA3ADUAMgBjAGQANwA3ADcANgA5ADkAYwA4ADkANAAyADQAZgAyAGMAMQA5ADIAOQAyADkAMwA4AGMAMABiAGYAMgBkAGEA
ZAA4ADkAOAAyADgANgAyAGIAYQBiADAANAA5AGIAMABhAGIAYQAyAGEAMwAwAGMAOAAzADkAMABhADMAMQBkADkAZgA0AGMAMAA1AGEAMQAxADAAZABmA
GQAMQA0AGYAYgBjAGYANAAyAGYAYwBiADUANwA3ADIAMgA1AGYAZQAzAGEAMAAyAGYAZABhADYAOAAwADkAZQAyADkAZQBmAGQANAA1AGUANgA5ADEAMw
BkAGUAYwBiAGQAOAAxAGMANwAzADkANAA3ADQAYwAwADEAMwAxADUANwA3ADkAOAAzADMAMwBjADgANwBhADUAMwAxADgAOAAyAGQAOAAwAGUAYQA4ADI
AZQA4AGEANwAyAGIAYQAyADEAZgAyADIAYQAwAGYAYwA5ADMANgAyADMAMQBhADcAOQBjADEAYQA0AGYAYwA3ADgAMQA5ADkAYwA1ADQAYgA1AGYAMAAz
ADcAMABiADkAOABjAGEAMAAwADYANgBiADcAZgAwADcAYwBmAGMANwBmADAAYgAxADIAOABhADAAZABjADAAOQA1ADkANAA5ADcAYQBkADIAYwBiADAAZ
gBjAGEANgAzADMAMAA4ADcANwA2ADcAMQBiADMAMwA3AGIAYwA0AGYANAAxAGIAZAA4AGIANgBjADYAMQA3ADcANQA1AGEANwA5ADUAMwAzADEANQBhAG
IAZQAwAGQAZQBiADQAMwAyAGMANwA2ADgAMgBkADEANQA3ADkAMwA4AGMAYwBiADMANgA0ADIAOQAyAGIAMABhAGIANQBjAGUANAA5ADEAZAA5AGUAOQB
kADkAMQAxADUAOQA2AGUAMQBmAGIAOABlADQAYwAzAGQAZAAxAGYAYQBmADMANwAxAGQANgBkAGMAZQAyAGUAYQBjADYAZAAzAGUA"

switch ($action) {
    0 { Set-DeploymentConfig -DeploymentType "Hybrid" -EncryptedJsonUrl $encryptedHybridUrl }
    1 { Set-DeploymentConfig -DeploymentType "AzureAD" -EncryptedJsonUrl $encryptedAADUrl }
    2 { Set-DeploymentConfig -DeploymentType "Travel PC" -EncryptedJsonUrl $encryptedTravelUrl }
    3 {
        Set-DeploymentConfig -DeploymentType "Donation PC"
        $Global:StartOSDCloudGUI += @{
            ApplyManufacturerDrivers   = $false
            ApplyCatalogDrivers        = $false
            ApplyCatalogFirmware       = $false
            AutopilotJsonChildItem     = $false
            AutopilotJsonItem          = $false
            AutopilotJsonName          = $false
            AutopilotJsonObject        = $false
            AutopilotOOBEJsonChildItem = $false
            AutopilotOOBEJsonItem      = $false
            AutopilotOOBEJsonName      = $false
            AutopilotOOBEJsonObject    = $false
            ImageFileFullName          = $false
            ImageFileItem              = $false
            ImageFileName              = $false
            OOBEDeployJsonChildItem    = $false
            OOBEDeployJsonItem         = $false
            OOBEDeployJsonName         = $false
            OOBEDeployJsonObject       = $false
            Restart                    = $false
            SkipAutopilot              = $true
            SkipAutopilotOOBE          = $true
            SkipODT                    = $true
            SkipOOBEDeploy             = $true
        }
        Start-OSDCloud
        Set-Volume -DriveLetter C -NewFileSystemLabel "Windows"
        Write-Host "Restarting in 10 seconds!" -ForegroundColor Cyan
        Start-Sleep -Seconds 10
        wpeutil reboot
        exit
    }
}

$imageFileUrl = Decrypt-SecureString $encryptedImageFileUrl

Start-OSDCloud -ImageFileUrl $imageFileUrl

Set-Volume -DriveLetter C -NewFileSystemLabel "Windows"

Write-Host "Restarting in 10 seconds!" -ForegroundColor Cyan
Start-Sleep -Seconds 10
wpeutil reboot